package com.abans.webapi.dao;

import com.abans.webapi.model.SubCategory;

import java.util.List;

public interface SubCategoryDao {
    public List<SubCategory> readAllByCategoryId(String id) throws Exception;
    public SubCategory findById(String id) throws Exception;
}
