package com.abans.webapi.dao;

import com.abans.webapi.model.Orders;

public interface OrderDao {
    public int save(Orders orders) throws Exception;
}
