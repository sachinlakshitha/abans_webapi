package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.CategoryDao;
import com.abans.webapi.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryDaoImpl implements CategoryDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Category> readAll() throws Exception {
        String sql = "SELECT * FROM CATEGORY";

        List<Category> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Category>>() {

                @Override
                public List<Category> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Category> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Category category = getCategory(resultSet);
                            listTemp.add(category);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            });

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    @Override
    public Category findById(String id) throws Exception {
        String sql = "SELECT * FROM CATEGORY WHERE CATEGORY_ID=?";

        List<Category> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Category>>() {

                @Override
                public List<Category> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Category> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Category category = getCategory(resultSet);
                            listTemp.add(category);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },id);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    private Category getCategory(ResultSet resultSet) throws SQLException, Exception {
        Category category = new Category();

        category.setId(resultSet.getString("CATEGORY_ID"));
        category.setName(resultSet.getString("CATEGORY_NAME"));

        return category;
    }
}
