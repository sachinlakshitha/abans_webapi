package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.OrderProductDao;
import com.abans.webapi.model.OrderProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderProductDaoImpl implements OrderProductDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(OrderProduct orderProduct) throws Exception {
        String sql = "INSERT INTO ORDER_PRODUCT (ORDER_PRODUCT_ID,ORDER_ID,PRODUCT_ID,QTY," +
                "UNIT_PRICE,DISCOUNT) VALUES (:id,:orderid,:productid,:qty,:unitprice,:discount)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", orderProduct.getId());
        mapSqlParameterSource.addValue("orderid", orderProduct.getOrderId());
        mapSqlParameterSource.addValue("productid", orderProduct.getProductId());
        mapSqlParameterSource.addValue("qty", orderProduct.getQty());
        mapSqlParameterSource.addValue("unitprice", orderProduct.getUnitPrice());
        mapSqlParameterSource.addValue("discount", orderProduct.getDiscount());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

}
