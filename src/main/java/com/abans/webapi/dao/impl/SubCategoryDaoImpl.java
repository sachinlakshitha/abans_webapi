package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.SubCategoryDao;
import com.abans.webapi.model.Category;
import com.abans.webapi.model.SubCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SubCategoryDaoImpl implements SubCategoryDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SubCategory> readAllByCategoryId(String id) throws Exception {
        String sql = "SELECT * FROM SUB_CATEGORY WHERE CATEGORY_ID=?";

        List<SubCategory> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<SubCategory>>() {

                @Override
                public List<SubCategory> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<SubCategory> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            SubCategory subCategory = getSubCategory(resultSet);
                            listTemp.add(subCategory);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },id);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    @Override
    public SubCategory findById(String id) throws Exception {
        String sql = "SELECT * FROM SUB_CATEGORY WHERE SUB_CATEGORY_ID=?";

        List<SubCategory> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<SubCategory>>() {

                @Override
                public List<SubCategory> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<SubCategory> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            SubCategory subCategory = getSubCategory(resultSet);
                            listTemp.add(subCategory);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },id);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    private SubCategory getSubCategory(ResultSet resultSet) throws SQLException, Exception {
        SubCategory subCategory = new SubCategory();

        subCategory.setId(resultSet.getString("SUB_CATEGORY_ID"));
        subCategory.setCategoryId(resultSet.getString("CATEGORY_ID"));
        subCategory.setName(resultSet.getString("SUB_CATEGORY_NAME"));

        return subCategory;
    }
}
