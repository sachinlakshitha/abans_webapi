package com.abans.webapi.dao;

import com.abans.webapi.model.Payment;

public interface PaymentDao {
    public int save(Payment payment) throws Exception;
}
