package com.abans.webapi.controller;

import com.abans.webapi.dto.CategoryDto;
import com.abans.webapi.dto.CategoryProductDto;
import com.abans.webapi.dto.ProductDto;
import com.abans.webapi.service.CategoryService;
import com.abans.webapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/subcategory/{name}")
    public ResponseEntity<List<ProductDto>> readAll(@PathVariable String name) throws Exception{
        return ResponseEntity.ok(productService.readAllBySubCategoryName(name));
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<ProductDto> getProductDetail(@PathVariable String id) throws Exception{
        return ResponseEntity.ok(productService.findById(id));
    }

    @GetMapping("/product/category")
    public ResponseEntity<List<CategoryProductDto>> getCategoryWithProducts() throws Exception{
        return ResponseEntity.ok(productService.readAllByCategory());
    }
}
