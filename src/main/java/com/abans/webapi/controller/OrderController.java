package com.abans.webapi.controller;

import com.abans.webapi.dto.OrdersDto;
import com.abans.webapi.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping(value = "")
    public ResponseEntity<Void> saveOrder(@RequestBody OrdersDto ordersDto) throws Exception {
        boolean isSaved = false;

        try {
            isSaved = orderService.save(ordersDto);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        if (isSaved) {
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
