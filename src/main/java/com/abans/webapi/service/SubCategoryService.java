package com.abans.webapi.service;

import com.abans.webapi.dto.SubCategoryDto;

import java.util.List;

public interface SubCategoryService {
    public List<SubCategoryDto> readAll() throws Exception;
}
