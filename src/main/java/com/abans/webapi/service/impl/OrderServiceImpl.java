package com.abans.webapi.service.impl;

import com.abans.webapi.dao.OrderDao;
import com.abans.webapi.dao.OrderProductDao;
import com.abans.webapi.dao.PaymentDao;
import com.abans.webapi.dto.OrderProductDto;
import com.abans.webapi.dto.OrdersDto;
import com.abans.webapi.dto.PaymentDto;
import com.abans.webapi.model.OrderProduct;
import com.abans.webapi.model.Orders;
import com.abans.webapi.model.Payment;
import com.abans.webapi.service.OrderService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderProductDao orderProductDao;
    @Autowired
    private PaymentDao paymentDao;
    @Autowired
    private ModelMapper mapper;

    @Override
    public boolean save(OrdersDto ordersDto) throws Exception {
        ordersDto.setId(UUID.randomUUID().toString());

        Orders order = getOrder(ordersDto);

        List<OrderProductDto> orderProductDtoList = ordersDto.getProducts();

        List<OrderProduct> orderProducts = new ArrayList<>();

        for (OrderProductDto orderProductDto:orderProductDtoList) {
            orderProductDto.setId(UUID.randomUUID().toString());
            orderProductDto.setOrderId(order.getId());

            orderProducts.add(getOrderProduct(orderProductDto));
        }

        Payment payment = getPayment(ordersDto.getPayment());

        payment.setId(UUID.randomUUID().toString());
        payment.setOrderId(order.getId());

        int isOrderSaved = orderDao.save(order);

        if(isOrderSaved == 1){
            for (OrderProduct orderProduct:orderProducts) {
                orderProductDao.save(orderProduct);
            }

            paymentDao.save(payment);

            return true;
        }

        return false;
    }

    private Orders getOrder(OrdersDto ordersDto) {
        Orders orders = mapper.map(ordersDto, Orders.class);
        return orders;
    }

    private OrdersDto getOrderDto(Orders order) {
        OrdersDto ordersDto = mapper.map(order, OrdersDto.class);
        return ordersDto;
    }

    private OrderProduct getOrderProduct(OrderProductDto orderProductDto) {
        OrderProduct orderProduct = mapper.map(orderProductDto, OrderProduct.class);
        return orderProduct;
    }

    private OrderProductDto getOrderProductDto(OrderProduct orderProduct) {
        OrderProductDto orderProductDto = mapper.map(orderProduct, OrderProductDto.class);
        return orderProductDto;
    }

    private Payment getPayment(PaymentDto paymentDto) {
        Payment payment = mapper.map(paymentDto, Payment.class);
        return payment;
    }

    private PaymentDto getPaymentDto(Payment payment) {
        PaymentDto paymentDto = mapper.map(payment, PaymentDto.class);
        return paymentDto;
    }
}
