package com.abans.webapi.service.impl;

import com.abans.webapi.dao.CategoryDao;
import com.abans.webapi.dao.SubCategoryDao;
import com.abans.webapi.dto.CategoryDto;
import com.abans.webapi.dto.SubCategoryDto;
import com.abans.webapi.model.Category;
import com.abans.webapi.model.SubCategory;
import com.abans.webapi.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private SubCategoryDao subCategoryDao;

    @Override
    public List<CategoryDto> readAll() throws Exception {
        List<CategoryDto> categoryDtoList = new ArrayList<>();

        List<Category> categories = categoryDao.readAll();

        for (Category category: categories) {
            CategoryDto categoryDto = getCategoryDto(category);

            List<SubCategory> subCategories = subCategoryDao.readAllByCategoryId(categoryDto.getId());

            List<SubCategoryDto> subCategoryDtos = subCategories.stream()
                    .map(this::getSubCategoryDto)
                    .collect(Collectors.toList());

            categoryDto.setSubCategories(subCategoryDtos);

            categoryDtoList.add(categoryDto);
        }

        return categoryDtoList;
    }

    private CategoryDto getCategoryDto(Category category) {
        CategoryDto categoryDto = mapper.map(category, CategoryDto.class);
        return categoryDto;
    }

    private SubCategoryDto getSubCategoryDto(SubCategory subCategory) {
        SubCategoryDto subCategoryDto = mapper.map(subCategory, SubCategoryDto.class);
        return subCategoryDto;
    }
}
